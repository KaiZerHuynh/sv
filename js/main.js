var dssv = [];

//lấy dữu liệu từ local Storage  lúc user load trang
var dataJson = localStorage.getItem("DSSV");
if(dataJson != null)
{
    dssv = JSON.parse(dataJson).map(function(item){
        return new SinhVien(item.ma,item.ten,item.email,item.matKhau,item.toan,item.ly,item.hoa);
    });
    //map
    renderDSSV(dssv);
}

function themSinhVien(){
    //lấy thông tin từ form
    var sv = layThongTinTuForm();
    dssv.push(sv);
    
    //render dssv: đưa data lên giao diện
    renderDSSV(dssv);

    //save dssv localStorage
    //localStorage : nơi lưu trữ (chỉ chấp nhận json), json: 1 loại dữ liệu
    var dataJson = JSON.stringify(dssv);
    localStorage.setItem("DSSV",dataJson);
}

function xoaSinhVien(id){
    var index = dssv.findIndex(function (item){
        return item.ma == id;
    });
    dssv.splice(index,1);
    renderDSSV(dssv);
}

function suaSinhVien(id){
    var index = dssv.findIndex(function (item){
        return item.ma == id;
    });
    //show thong tin len form
    showThongTinLenForm(dssv[index]);
}

function capNhat(id){
    var index = dssv.findIndex(function (item){
        return item.ma == id;
    });

    renderDSSV(dssv)
}

//findIndex:tìm ra vị trí phải có 1 hàm với giá trị trả về
//slice: sao chép mảng 
//splice: cắt mảng (vị trí, số ptu muốn xóa)
//array map
//call back function
